$.get('./cards.json', (data) => {
    const images = data.cards;
    const moreImages = data.moreCards;
    images.map(item => {
        $(".works .images").append(`
         <a class="category shown" data-category="${item.category}" href="#">
                    <div class="dis-el">
                        <div class="dis-el-but">
                            <button><i class="fas fa-link"></i></button>
                            <button><i class="fas fa-search"></i></button>
                        </div>
                        <p class="dis-el-p1">creative design</p>
                        <p class="dis-el-p2">Web Design</p>
                    </div>
                                   <img src="${item.url}" alt="img">
                </a>
         `)
    })

    $('.sec3-button').click((e) => {
        e.preventDefault();
        moreImages.map(item => {
            $(".works .images").append(`
         <a class="category shown" data-category="${item.category}" href="#">
                    <div class="dis-el">
                        <div class="dis-el-but">
                            <button><i class="fas fa-link"></i></button>
                            <button><i class="fas fa-search"></i></button>
                        </div>
                        <p class="dis-el-p1">creative design</p>
                        <p class="dis-el-p2">Web Design</p>
                    </div>
                                   <img src="${item.url}" alt="img">
                </a>
         `)
        })
        $('.sec3-button').remove();
    })
});


$('.tabs-services p').click(e => {
    const tabs = $('.tabs-services p');
    const content = $('.tabs-content p');

    tabs.removeClass('active');
    content.removeClass('active');

    $(e.target).addClass('active');
    content.filter(`[data-id="${$(e.target).data('id')}"]`).addClass('active');
});


$('.tabs-works p').click(e => {
    const tabs = $('.tabs-works p');
    const content = $('.images .category');

    tabs.removeClass('active');
    content.removeClass('shown');

    $(e.target).addClass('active');
    $(e.target).data('category') === 1 ? content.addClass('shown') :
        content.filter(`[data-category="${$(e.target).data('category')}"]`).addClass('shown');
});


$.get('./peoples.json', (data) => {
    data.map(item => {
        $('.people-say .list').append(`
            <img src="${item.img}" data-category="${item.id}"></img>
        `)
    });


    $(".people-say .list img").click((e) => {
        data.map(item => {
            if (item.id === $(e.target).data('category')) {
                $('.people-say').fadeToggle(125);


                setTimeout(() => {
                    $('.people-say .userText').html(item.text);
                    $('.people-say .selected').html(`
                <h3 data-id="${item.id}">${item.name}</h3>
                        <p>${item.position}</p>
                      <img src="${item.img}">
                `)
                    $('.people-say').fadeToggle(125);

                }, 250);

            }
        })
    });
})


